import React from "react";

function ChildHolder(props) {
  return (
    <div className="parent-color-holder">
      {props.getElement.map((innerele, i) => {
        return (
          <>
            <div
              className="innerDiv"
              key={innerele}
              style={{ backgroundColor: innerele }}
            >
              <div className="number-holder">
                <div className="number">{i == 0 ? "50" : i * 100}</div>
                <div className="text">{innerele.toUpperCase()}</div>
              </div>
            </div>
          </>
        );
      })}
    </div>
  );
}

export default ChildHolder;
