import React from "react";
import ReactDOM from "react-dom/client";
import Color from "./color.js";
import "./style.css";
import data from "./colors.json";
function App() {
  return (
    <>
      <Color data={data} />
    </>
  );
}

let root = ReactDOM.createRoot(document.querySelector("#root"));
root.render(<App />);
