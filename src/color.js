import React from "react";
import ChildHolder from "./childHolder";

function Color(props) {
  return (
    <div className="main-div">
      {Object.keys(props.data).map((ele) => {
        return (
          <div key={ele} className="colorBox">
            <div className="color-name">
              {ele.charAt(0).toUpperCase() + ele.slice(1)}
              <br />
              <div className="small-name">{`color.${ele}`}</div>
            </div>
            <ChildHolder getElement={props.data[ele]} />
          </div>
        );
      })}
    </div>
  );
}

export default Color;
